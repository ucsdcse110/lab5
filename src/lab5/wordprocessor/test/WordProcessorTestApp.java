package lab5.wordprocessor.test;

import lab5.wordprocessor.java.LowerPrinter;
import lab5.wordprocessor.java.ProtectedDocument;
import lab5.wordprocessor.java.UpperPrinter;
import lab5.wordprocessor.java.WordProcessor;

//import lab5.wordprocessor.java.NormalStatisticsPrinter;

public class WordProcessorTestApp {

	public static void main(String[] args) {
		WordProcessor wp = new WordProcessor();
		
		wp.setPrintMode(new UpperPrinter());
		
		ProtectedDocument doc = new ProtectedDocument("myDoc", "C:\\Folder\\");
		
		doc.addNewLine("Hello");
		doc.addNewLine("wOrLd");
		
		wp.AddDocument(doc);
		
		wp.printDocument(0);
		
		wp.setPrintMode(new LowerPrinter());
		
		wp.printDocument(0);
	}

	/*
	 public static void main(String[] args) {
		WordProcessor wp = new WordProcessor();
		
		wp.setPrintMode(new UpperPrinter());
		
		ProtectedDocument doc = new ProtectedDocument("myDoc", "C:\\Folder\\");
		wp.AddDocument(doc);
		
		doc.getPages().get(0).addNewLine("Hello");
		doc.getPages().get(0).addNewLine("wOrlD");
		
		wp.printDocument(0);
		
		wp.setPrintMode(new LowerPrinter());
		
		wp.printDocument(0);
		
		wp.setPrintMode(new NormalStatisticsPrinter());
		
		wp.printDocument(0);
		
	}

	 */
}
