package lab5.wordprocessor.java;

public class SizableDocument extends Document {
	private float height;
	private float weight;
	public float getHeight() {
		return height;
	}
	public void setHeight(float height) {
		this.height = height;
	}
	public float getWeight() {
		return weight;
	}
	public void setWeight(float weight) {
		this.weight = weight;
	}
}
