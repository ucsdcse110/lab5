package lab5.wordprocessor.java;

import java.util.ArrayList;
import java.util.List;

public class WordProcessor {

	private IPrinter printer;
	List<Document> documents = new ArrayList<Document>();

	public IPrinter getPrintMode() {
		return printer;
	}

	public void setPrintMode(IPrinter printMode) {
		this.printer = printMode;
	}

	public void AddDocument(Document doc){
		documents.add(doc);
	}
	
	public void printDocument(int index){
		printer.print(documents.get(index).getLines());
	}
}
