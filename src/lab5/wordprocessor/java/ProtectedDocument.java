package lab5.wordprocessor.java;

public class ProtectedDocument extends Document {
	private String password;

	public ProtectedDocument(String pName, String pLocation) {
		super(pName, pLocation);
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
}
