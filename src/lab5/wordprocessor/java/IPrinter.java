package lab5.wordprocessor.java;

import java.util.List;

public interface IPrinter {

	public void print(List<String> lines);

}
