package lab5.wordprocessor.java;

import java.util.List;

public class LowerPrinter implements IPrinter {

	@Override
	public void print(List<String> lines) {
		for(String line : lines){
			System.out.println(line.toLowerCase());
		}

	}

}
