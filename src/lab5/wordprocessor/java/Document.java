package lab5.wordprocessor.java;

import java.util.ArrayList;
import java.util.List;

public abstract class Document {
	private String name;
	private int sizeKB;
	private String location;
	private List<String> lines = new ArrayList<String>();
	
	public Document(){
		// change this to add a default page instead	
		String defaultLine = new String();
		lines.add(defaultLine);
	}
	public Document(String pName, String pLocation){
		name = pName;
		location = pLocation;
		// change this to add a default page instead
		String defaultLine = new String();
		lines.add(defaultLine);
	}
	
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public int getSizeKB() {
		return sizeKB;
	}
	public void setSizeKB(int sizeKB) {
		this.sizeKB = sizeKB;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	// change this to add a page
	public void addNewLine(String newLine){
		lines.add(newLine);
	}
	
	// change this to return list of pages instead
	public List<String> getLines(){
		return lines;
	}
	
	
	
}
